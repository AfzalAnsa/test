//
//  Model.swift
//  Tester
//
//  Created by Tanzeel Khan on 2/10/20.
//  Copyright © 2020 afzal. All rights reserved.
//

import Foundation

struct EmailData:Codable {
    var items:[items]?
}

struct items:Codable {
   var emailId: String?
    var lastName: String?
    var imageUrl : String?
    var firstName : String?
}
