//
//  ApiCall.swift
//  Tester
//
//  Created by Tanzeel Khan on 2/10/20.
//  Copyright © 2020 afzal. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

class appServer {
    enum GetFriendsFailureReason: Int, Error {
        case unAuthorized = 401
        case notFound = 404
    }
    func getFriends(emailId:String) -> Observable<EmailData> {
     let paramaters = ["emailId": emailId]
    return Observable.create { observer -> Disposable in
        AF.request("http://surya-interview.appspot.com/list", method: .post, parameters: paramaters, encoding: JSONEncoding.default)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        // if no error provided by alamofire return .notFound error instead.
                        // .notFound should never happen here?
                        observer.onError(response.error ?? GetFriendsFailureReason.notFound)
                        return
                    }
                    do {
                        let friends = try JSONDecoder().decode(EmailData.self, from: data)
                        observer.onNext(friends)
                    } catch {
                        observer.onError(error)
                    }
                case .failure(let error):
                    if let statusCode = response.response?.statusCode,
                        let reason = GetFriendsFailureReason(rawValue: statusCode)
                    {
                        observer.onError(reason)
                    }
                    observer.onError(error)
                }
        }
 
        return Disposables.create()
    }
}
}
