//
//  EmailVc.swift
//  Tester
//
//  Created by Tanzeel Khan on 2/11/20.
//  Copyright © 2020 afzal. All rights reserved.
//

import UIKit

class EmailVc: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var emailText:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
         emailText.resignFirstResponder()
    }
    @objc func dismissKeyboard() {
        emailText.resignFirstResponder()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text!.isEmpty {
            return false
        }else {
            let vc = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            vc.passEmail = textField.text ?? ""
            self.present(vc, animated: true, completion: nil)
            textField.resignFirstResponder()
            return true
        }
    }
    
    
    
}
