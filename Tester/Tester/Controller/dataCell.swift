//
//  dataCell.swift
//  Tester
//
//  Created by Tanzeel Khan on 2/10/20.
//  Copyright © 2020 afzal. All rights reserved.
//

import UIKit

class dataCell: UITableViewCell {
    
    @IBOutlet weak var nameLbl:UILabel!
    @IBOutlet weak var profileImage:UIImageView!
    @IBOutlet weak var emailLbl:UILabel!
    @IBOutlet weak var viewImage:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewImage.RoundedCircle()
        profileImage.RoundedCircle()
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension UIView {
    func RoundedCircle() {
        self.layer.cornerRadius = 30
        self.clipsToBounds = true
    }
}
