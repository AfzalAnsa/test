//
//  ViewController.swift
//  Tester
//
//  Created by Tanzeel Khan on 2/10/20.
//  Copyright © 2020 afzal. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift
import RxCocoa
import SDWebImage
import RealmSwift
class ViewController: UIViewController {
    @IBOutlet weak var tableView:UITableView!
    
    var email = EmailData()
    var storeEmail = Email()
    var passEmail = String()
    private let disposeBag = DisposeBag()
    var specimens = try! Realm().objects(Email.self)
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "dataCell", bundle: nil), forCellReuseIdentifier: "dataCell")
        tableView.delegate = self
        tableView.dataSource = self
        getData()
        
        
    }
    
    func getData() {
        let app = appServer()
        app.getFriends(emailId: passEmail).subscribe(
            onNext: { [weak self] friends in
                // Store value
                self?.email = friends
                for i in 0...friends.items!.count - 1 {
                    let emaildata = Email()
                    emaildata.email = friends.items![i].emailId!
                    emaildata.name = friends.items![i].firstName!
                    emaildata.lastname = friends.items![i].lastName!
                    emaildata.image = friends.items![i].imageUrl!
                    
                    let realm = try! Realm()
                    // You only need to do this once (per thread)
                    
                    // Add to the Realm inside a transaction
                    try! realm.write {
                        realm.add(emaildata, update: .all)
                        
                    }
                   
                }
                self?.tableView.reloadData()
                
            },
            onError: { [weak self] error in
                // Present error
            }
        )
            .disposed(by: disposeBag)
    }
}

extension ViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = email.items?.count else {return specimens.count}
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell", for: indexPath) as! dataCell
        
        if email.items?.count != nil {
            cell.nameLbl?.text = "\(String(describing: email.items![indexPath.row].firstName!))" + " " + "\(String(describing: email.items![indexPath.row].lastName!))"
            cell.emailLbl?.text = email.items?[indexPath.row].emailId
            cell.profileImage.sd_setImage(with: URL(string: (email.items?[indexPath.row].imageUrl!)!), placeholderImage: UIImage(named: "avatar.jpg"))
        }
        else {
            cell.nameLbl?.text =  "\(specimens[indexPath.row].name))" + " " + "\(specimens[indexPath.row].name))"
            cell.emailLbl?.text = specimens[indexPath.row].email
            cell.profileImage.sd_setImage(with: URL(string: (specimens[indexPath.row].image)), placeholderImage: UIImage(named: "avatar.jpg"))
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

class Email: Object {
    @objc dynamic var name = ""
    @objc dynamic var lastname = ""
    @objc dynamic var email = ""
    @objc dynamic var image = ""
    override static func primaryKey() -> String? {
        return "email"
    }
}
